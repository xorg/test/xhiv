/*
 * Copyright (c) 2013, 2023, Oracle and/or its affiliates.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "xhiv.h"
#include <X11/Xlib.h>
#include <X11/Xlibint.h>
#include "xhiv-Xrandr.h"
#include <assert.h>
#include <stdio.h>
#include <limits.h>

static void
testOverflowFields(void)
{
    const xQueryExtensionReply overflow_xrandr_qext_reply = {
        .type = X_Reply,
        .length = 0,
        .present = xTrue,
        .major_opcode = MY_XRANDR_EXT_CODE,
        .first_event = 255, /* Only events < 128 are allowed in protocol */
        .first_error = 255
    };
    Display *dpy, *saved_dpy;
    int major = MY_XRANDR_MAJOR_VERSION;
    int minor = MY_XRANDR_MINOR_VERSION;
    int status;

    xrandr_qext_response.response_data = &overflow_xrandr_qext_reply;

    dpy = XhivOpenDisplay(&xrandr_vers_response);
    saved_dpy = calloc(1, sizeof(Display));
    assert(saved_dpy != NULL);
    memcpy(saved_dpy, dpy, sizeof(Display));

    printf("XRRQueryVersion: overflow event id test\n");
    status = XRRQueryVersion(dpy, &major, &minor);
    assert(status != 0);
    /* check that event_vec didn't overflow into wire_vec */
    for (int i = 0 ; i < 127; i++) {
        assert(dpy->wire_vec[i] == saved_dpy->wire_vec[i]);
    }
    /* check that wire_vec didn't overflow into following field */
    assert(dpy->lock_meaning == saved_dpy->lock_meaning);

    XhivCloseDisplay(dpy);
}

int
main(int argc, char **argv)
{
    testOverflowFields();
    printf("XRRQueryVersion: all tests passed\n");
    return 0;
}
